require 'sinatra/base'
require 'active_record'

class PictureHostingApp < Sinatra::Base
	set :sessions, true
	set :root, File.dirname(__FILE__)
	#set :public_folder, Proc.new { File.join(root, "public") }
	set :public, 'public'
	set :static, true
	set :files,  Proc.new { File.join(settings.public_folder, 'uploads') }
	set :views, Proc.new { File.join(root, "views") }
	#set :show_exceptions, false
	#set :raise_errors, false

	ActiveRecord::Base.establish_connection(
		:adapter => 'sqlite3',
		:database =>  'db/pics.sqlite3.db'
	)

	class Picture < ActiveRecord::Base
	end

	get '/' do
		@title = "Загрузить"
		erb :index
	end

	post '/' do
		lastpic = Picture.last
		tempfile = params[:file][:tempfile]
		filename = params[:file][:filename]
		new_id = lastpic.id + 1
		new_filename = new_id.to_s + File.extname(tempfile)
		File.open("public/uploads/" + new_filename.to_s, "w") do |f|
			f.write(tempfile.read)
		end
		pic = Picture.new
		pic.name = filename
		pic.save
		redirect to("/pic/#{new_id}")
		# do something ...
	end

	get '/gallery' do
		@title = "Галерея"
		erb :gallery
	end

	get '/gallery/:num' do
		@title = "Галерея, страница #{params[:num]}"
		if params[:num] == 1 then
			erb :gallery
		elsif params[:num] < 1 then
			redirect to('/404')
		end
	end

	get '/pic/:num' do
		@picture = Picture.find(params[:num])
		@title = @picture.name 
		@pic = "/uploads/" + params[:num] + File.extname(@picture.name)
		erb :pic
	end
#	get '/404' do
#		erb :404
#	end

	#get '/static/uploads/:file' do
	#	send_file(settings.files + params[:file])
	#end

	run! if app_file == $0
end